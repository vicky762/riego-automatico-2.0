#include <Arduino.h>
#include "MoistureSensor.hpp"
#include "Pumps.hpp"
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#define TICK 200

ClsAnalogSensor Sensor1(0,451,855);
ClsPumps Pump1(22);
LiquidCrystal_I2C lcd(0x27, 16, 2);



TplantData Conchita1;

void setup() {

  //analogReference(EXTERNAL);
  Serial.begin(9600);
   lcd.begin();                      
   lcd.backlight();
   lcd.setCursor(0, 0);
   lcd.print("IRRIGATION");

}

void loop() {

  
  if(millis() % TICK == 0){
    Conchita1.PercentageIrrigation = Sensor1.MetGetPercentage();
    Conchita1.PumpState = Pump1.MetAutoIrrigation(true, Conchita1.PercentageTriggerValue,Conchita1.PercentageStopValue,Conchita1.PercentageIrrigation);
    lcd.setCursor(0,1);
    lcd.print("H1 = ");
    lcd.print(Conchita1.PercentageIrrigation);
    lcd.print(" %");
    if(Conchita1.PumpState){
    lcd.print(" |!|");
    }
    else{
      lcd.setCursor(9,1);
      lcd.print("    ");
    }
  }

}