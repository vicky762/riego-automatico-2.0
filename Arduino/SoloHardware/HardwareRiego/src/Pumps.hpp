#include <Arduino.h>


typedef struct{
  short PercentageIrrigation = 0; //Store current irrigation percentage here
  bool PumpState = false; //Store PumpState here
  short PercentageTriggerValue = 55; //Start irrigation percentage
  short PercentageStopValue = 85; //Stop irrigation percentage
} TplantData;

class ClsPumps
{
private:
    short _DO; //LInked digital output
    bool _AutoIrrigation = false; //Automatic irrigation in progress
    bool _TimedIrrigation = false; //Timed irrigation in progress
    unsigned long _IrrigationTime = 15000; //Irrigation time
public:
    ClsPumps(short DO); //Constructor
    bool MetTimedIrrigation(bool enable, bool trigger, unsigned long IrrigationTime);//Irrigation with timers
    bool MetAutoIrrigation(bool enable, unsigned int TriggerValue, unsigned int StopValue, unsigned int CurrentValue);//Fully luxurious gay space automated communist irrigation
};

ClsPumps::ClsPumps(short DO){
    _DO = DO;
    pinMode(_DO, OUTPUT); //Set pin to output
    digitalWrite(_DO,HIGH); //Force to HIGH so relay keeps off
}

bool ClsPumps::MetTimedIrrigation(bool enable, bool trigger, unsigned long IrrigationTime){
    unsigned long LULongTimers[2];
    if(enable){ //enable signal, method only works if enable is true
        if(trigger && !_TimedIrrigation){ //Starting timed irrigation
            LULongTimers[0] = millis();//Start time
            LULongTimers[1] = LULongTimers [0];
        }
        if(_TimedIrrigation && (IrrigationTime >= (LULongTimers[1] - LULongTimers [0]))){ //If time is counting, keeps irrigating
            _TimedIrrigation = true; //Control variable which shows timed irrigation state
            LULongTimers[1] = millis(); //Updates timer
        }
        else if(_TimedIrrigation && (IrrigationTime < (LULongTimers[1] - LULongTimers [0]))){ //Timer goes off
            _TimedIrrigation = false; //reboots variables
            LULongTimers[0] = 0;
            LULongTimers[1] = 0;
        }
    }
    else{ //If enable is false, reboots everything
       _TimedIrrigation = false;
       LULongTimers[0] = 0;
       LULongTimers[1] = 0;
    }
    digitalWrite(_DO,!_TimedIrrigation); //updates the output status
    return _TimedIrrigation;
}

bool ClsPumps::MetAutoIrrigation(bool enable, unsigned int TriggerValue, unsigned int StopValue,unsigned int CurrentValue){

    if(enable){
        if(!_AutoIrrigation){
            _AutoIrrigation = (TriggerValue >= CurrentValue) ? true : false; //if current value is more than Trigger, it starts irrigating until stop value is reached
        }
        else{
            _AutoIrrigation = (StopValue <= CurrentValue) ? false : true; //Stop value has been reached
        }
    }
    else{
        _AutoIrrigation = false;
    }
    digitalWrite(_DO,!_AutoIrrigation);
    return _AutoIrrigation;
}

/*
Timed irrigation irrigates during a period of time in milliseconds and it's used when sensor readings are unrelaible to stop
Usually, automatic irrigation is used. In auto, system waits until humidity is bellow a setpoint. Once that SP is reached, DO is set to true. 
I will only be disabled when a second setpoint is reached. We can say setpoint 1 is minimun humidity and setpoint 2 is maximum.

This values are calculated with the signal previously escalated.
*/

