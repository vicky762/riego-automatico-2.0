#include <Arduino.h>


class ClsAnalogSensor{ //This parameters are for analg reading values
    private:
        short _Apin; //Assigned analog pin
        unsigned int _min,_max; //Min and max values for analog sensor (test it first)
    public:
        ClsAnalogSensor(short Apin,unsigned int min,unsigned int max); //Constructor
        unsigned int    MetGetAnalogReading();//Gets raw analog value between max and min
        unsigned int    MetGetVolts();//Gets voltage in analog input in mV
        byte MetGetPercentage();
};

ClsAnalogSensor::ClsAnalogSensor(short Apin,unsigned int min,unsigned int max){
    _Apin = Apin;
    _min = min;
    _max = max;
}
unsigned int ClsAnalogSensor::MetGetAnalogReading(){
    unsigned int _AnalogReading = analogRead(_Apin);
    _AnalogReading = constrain(_AnalogReading,_min,_max);
    return _AnalogReading;
}
unsigned int ClsAnalogSensor::MetGetVolts(){
    unsigned int _Volts = analogRead(_Apin);
    const int _resolution = 1023, VoltReference = 5000;

     /*
    _resolution and VoltReference have been set to default ADC parameters from Arduino AVR boards. Remember to change if you are using any other MCUs
    */

    _Volts *= VoltReference;
    _Volts /= _resolution;
     return _Volts;
}
byte ClsAnalogSensor::MetGetPercentage(){
    return (100 - map(MetGetAnalogReading(),_min,_max,0,100));
}

