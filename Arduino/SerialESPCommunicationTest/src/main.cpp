#include <Arduino.h>
#include <SoftwareSerial.h>
#define PIN A1
#define InLed 13

SoftwareSerial ESPSerial(6,5); //Tx, Rx

uint16_t analogValue = 0;

void FncDoPing(){
  unsigned long timer[]={4000,0,0};
  byte LByteInput = 0;
  bool LBCheck = false;
  timer[1] = millis();
  timer[2] = timer[1];

  while(timer[2] - timer[1] < timer[0]){
    ESPSerial.print(8);
    if(ESPSerial.available()){
      LByteInput = ESPSerial.read();
      //Serial.println(LByteInput);
      if(LByteInput == '8'){
        LBCheck = true;
      }
    }
    timer[2] = millis();
  }

  if(LBCheck){
    Serial.println("Ping Succesfull nwn");
    digitalWrite(InLed,HIGH);
  }
  else{
    Serial.println("Ping lost TwT");
  }

}

void setup() {
  pinMode(InLed,OUTPUT);
  digitalWrite(InLed,LOW);
  ESPSerial.begin(115200);
  Serial.begin(115200);
  FncDoPing();
}

void loop() {
  
}