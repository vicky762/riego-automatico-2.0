#include <Arduino.h>
#include <SoftwareSerial.h>
#define D5 14
#define D6 12

SoftwareSerial InputSerial(D5,D6);// Tx, Rx
byte InByte = 0;

void HostServices(byte RequestID){

  switch (RequestID)
  {
  case 56: //ping
    InputSerial.print(8);
    Serial.println("Ping Request Received");
    break;

  case 0:
    break;
  default:
    Serial.println("Unknown Request");
    break;
  }

}


void setup() {
  
  Serial.begin(115200);
  InputSerial.begin(115200);
}

void loop() {

  if(InputSerial.available()>0){
    InByte = InputSerial.read();
    Serial.println(InByte);
    HostServices(InByte);
  }

  InByte = (InByte > 0) ? 0 : InByte;
}